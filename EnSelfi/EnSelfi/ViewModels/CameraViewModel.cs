﻿using System;
using System.Collections.Generic;
using System.Text;
using EnSelfi.Models;
using Xamarin.Forms;
using Plugin.Media;

namespace EnSelfi.ViewModels
{
    public class CameraViewModel : BaseViewModel
    {
        PhotoDef _currentPhoto;

        ImageSource imageSourceOfPhoto = null;
        public ImageSource ImageSourceOfPhoto
        {
            get { return imageSourceOfPhoto; }
            set { SetProperty(ref imageSourceOfPhoto, value); }
        }

        public Command TakePicture { get; set; }

        public CameraViewModel()
        {
            Title = "Photo";
            _currentPhoto = new PhotoDef();
            TakePicture = new Command(() => ExecuteTakePictureCommad());
        }

        async void ExecuteTakePictureCommad()
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {                
                await Application.Current.MainPage.DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg"
            });
            if (file == null)
                return;
            
            this._currentPhoto.PathOfPhoto = file.Path.ToString();
            await Application.Current.MainPage.DisplayAlert("File Location", file.Path, "OK");

            this.ImageSourceOfPhoto = ImageSource.FromStream(() =>
            {
                var stream1 = file.GetStream();
                file.Dispose();
                return stream1;
            });
            this._currentPhoto.StreamOfPhoto = this.ImageSourceOfPhoto;
            this.RefreshProperty();
        }

        private void RefreshProperty()
        {
            OnPropertyChanged("ImageSourceOfPhoto");
        }

    }
}
