﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EnSelfi.Models
{
    public class PhotoDef
    {
        public string PathOfPhoto { get; set; }
        public ImageSource StreamOfPhoto { get; set; }
    }
}
