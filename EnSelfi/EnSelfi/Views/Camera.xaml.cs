﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Media;
using System.Reflection;
using System.IO;
using EnSelfi.ViewModels;

namespace EnSelfi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Camera : ContentPage
    {

        CameraViewModel viewModel;

        public Camera()
        {
            BindingContext = viewModel = new CameraViewModel();

            InitializeComponent();
        }

        async void TakePhoto_Clicked(object sender, System.EventArgs e)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg"
            });
            if (file == null)
                return;
            await DisplayAlert("File Location", file.Path, "OK");

            //Stream fileStream = File.OpenRead(file.Path); // open a stream to an image file

            image.Source = ImageSource.FromStream(() =>
            {
                var stream1 = file.GetStream();
                file.Dispose();
                return stream1;
            });

        }
    }
}